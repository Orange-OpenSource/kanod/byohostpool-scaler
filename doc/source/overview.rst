Overview
========
Byohostpool-Scaler is reponsible of scaling the Byohostpool resources.

It watches `KubeadmControlPlane` and `MachineDeployment` resources
and adapts the replicas size of the Byohostpool resources consequently.
