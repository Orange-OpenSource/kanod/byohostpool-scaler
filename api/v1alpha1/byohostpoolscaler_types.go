/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ByoHostPoolScalerSpec defines the desired state of ByoHostPoolScaler
type ByoHostPoolScalerSpec struct {
	// cluster name
	Cluster string `json:"cluster"`
	// os name of the provisioning image for BareMetalHost
	ByohOs string `json:"byohOs"`
	// os version of the provisioning image for BareMetalHost
	ByohOsVersion string `json:"byohOsVersion"`
	// format of the provisioning image for BareMetalHost
	ByohImageFormat string `json:"byohImageFormat"`
	// repository of the provisioning image for BareMetalHost
	ByohRepository string `json:"byohRepository"`
	// api server address
	ApiServer string `json:"apiServer"`
	// insecureSkipTLSVerify mode for generating bootstrapkubeconfig
	InsecureSkipTLSVerify bool `json:"insecureSkipTLSVerify"`
}

// ByoHostPoolScalerStatus defines the observed state of ByoHostPoolScaler
type ByoHostPoolScalerStatus struct {
	// List of the provisioned BareMetalHost with ByoHost image
	ByoHostPoolList []string `json:"byoHostPoolList,omitempty"`
	// Status of the byohpool
	Status string `json:"status"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// ByoHostPoolScaler is the Schema for the byohostpoolscalers API
type ByoHostPoolScaler struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ByoHostPoolScalerSpec   `json:"spec,omitempty"`
	Status ByoHostPoolScalerStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ByoHostPoolScalerList contains a list of ByoHostPoolScaler
type ByoHostPoolScalerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ByoHostPoolScaler `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ByoHostPoolScaler{}, &ByoHostPoolScalerList{})
}
