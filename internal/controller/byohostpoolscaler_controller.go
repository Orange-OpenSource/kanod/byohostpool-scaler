/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"time"

	"github.com/go-logr/logr"

	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"

	capi "sigs.k8s.io/cluster-api/api/v1beta1"
	controlplane "sigs.k8s.io/cluster-api/controlplane/kubeadm/api/v1beta1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	poolsscalerv1alpha1 "gitlab.com/Orange-OpenSource/kanod/byohostpool-scaler/api/v1alpha1"
	poolsv1alpha1 "gitlab.com/Orange-OpenSource/kanod/byohostpool/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	logger "sigs.k8s.io/controller-runtime/pkg/log"
)

// ByoHostPoolScalerReconciler reconciles a ByoHostPoolScaler object
type ByoHostPoolScalerReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	Log    logr.Logger
}

type ByoHostPoolTargetType map[string]map[string]int32

const (
	LABEL_BAREMETALPOOL                  = "kanod.io/baremetalpool"
	LABEL_CAPI_CLUSTERNAME               = "cluster.x-k8s.io/cluster-name"
	LABEL_CAPI_MACHINE_DEPLOYMENT_NAME   = "cluster.x-k8s.io/deployment-name"
	bhpscalerAnnotationPaused            = "byohostpoolscaler.kanod.io/paused"
	MilliResyncPeriod                    = 10000
	apiServerByoHostPoolScalerAnnotation = "kanod.io/apiserver"
)

//+kubebuilder:rbac:groups=pools.kanod.io,resources=byohostpoolscalers,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=pools.kanod.io,resources=byohostpoolscalers/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=pools.kanod.io,resources=byohostpoolscalers/finalizers,verbs=update
//+kubebuilder:rbac:groups=controlplane.cluster.x-k8s.io,resources=kubeadmcontrolplanes,verbs=get;list;watch
//+kubebuilder:rbac:groups=cluster.x-k8s.io,resources=machinedeployments,verbs=get;list;watch
//+kubebuilder:rbac:groups=cluster.x-k8s.io,resources=machinesets,verbs=get;list;watch
//+kubebuilder:rbac:groups=pools.kanod.io,resources=byohostpools,verbs=get;list;watch;create;update;patch;delete

func (r *ByoHostPoolScalerReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logger.Log.WithValues()

	var byoHostPoolTarget ByoHostPoolTargetType = make(ByoHostPoolTargetType)

	byoHostPoolScaler := &poolsscalerv1alpha1.ByoHostPoolScaler{}
	err := r.Get(ctx, req.NamespacedName, byoHostPoolScaler)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("byoHostPoolScaler resource not found. Ignoring")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get byoHostPoolScaler")
		return ctrl.Result{}, err
	}

	annotations := byoHostPoolScaler.GetAnnotations()
	if annotations != nil {
		if _, ok := annotations[bhpscalerAnnotationPaused]; ok {
			log.Info("byohostpool-scaler is paused")
			return ctrl.Result{Requeue: false}, nil
		}
	}

	err = r.ComputeKcpReplicas(ctx, byoHostPoolScaler, &byoHostPoolTarget)
	if err != nil {
		log.Error(err, "Cannot compute scale for kcp")
		return ctrl.Result{}, err
	}

	err = r.ComputeMachineDeploymentReplicas(ctx, byoHostPoolScaler, &byoHostPoolTarget)
	if err != nil {
		log.Error(err, "Cannot compute scale for kcp")
		return ctrl.Result{}, err
	}

	err = r.createByoHostPoolResource(ctx, byoHostPoolScaler, &byoHostPoolTarget)
	if err != nil {
		log.Error(err, "Cannot create ByoHostPool resources")
		return ctrl.Result{}, err
	}

	return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
}

func initPoolTarget(byoHostPoolTarget *ByoHostPoolTargetType, key string) {
	if byoHostPoolTarget == nil {
		return
	}

	_, ok := (*byoHostPoolTarget)[key]
	if !ok {
		(*byoHostPoolTarget)[key] = map[string]int32{}
	}
}

func (r *ByoHostPoolScalerReconciler) ComputeKcpReplicas(
	ctx context.Context,
	byoHostPoolScaler *poolsscalerv1alpha1.ByoHostPoolScaler,
	byoHostPoolTarget *ByoHostPoolTargetType,
) error {
	clusterName := byoHostPoolScaler.Spec.Cluster
	kacpList := &controlplane.KubeadmControlPlaneList{}
	if err := r.List(ctx, kacpList, client.MatchingLabels{LABEL_CAPI_CLUSTERNAME: clusterName}, client.InNamespace(byoHostPoolScaler.Namespace)); err != nil {
		r.Log.Error(err, "cannot list control planes")
		return err
	}
	for _, kacp := range kacpList.Items {
		lbs := kacp.GetLabels()
		bmpool, ok := lbs[LABEL_BAREMETALPOOL]
		if !ok {
			err := fmt.Errorf("missing label in kcp %s : %s", kacp.Name, LABEL_BAREMETALPOOL)
			return err
		}

		initPoolTarget(byoHostPoolTarget, bmpool)

		if kacp.Status.Version != nil && *(kacp.Status.Version) != kacp.Spec.Version {
			// kcp in upgrading process
			(*byoHostPoolTarget)[bmpool][kacp.Spec.Version] += kacp.Status.UpdatedReplicas
			(*byoHostPoolTarget)[bmpool][*kacp.Status.Version] += kacp.Status.Replicas - kacp.Status.UpdatedReplicas
		} else {
			(*byoHostPoolTarget)[bmpool][kacp.Spec.Version] += int32(*kacp.Spec.Replicas)
		}
	}
	return nil
}

func (r *ByoHostPoolScalerReconciler) ComputeMachineDeploymentReplicas(
	ctx context.Context,
	byoHostPoolScaler *poolsscalerv1alpha1.ByoHostPoolScaler,
	byoHostPoolTarget *ByoHostPoolTargetType,
) error {
	clusterName := byoHostPoolScaler.Spec.Cluster
	mdList := &capi.MachineDeploymentList{}
	if err := r.List(ctx, mdList, client.MatchingLabels{LABEL_CAPI_CLUSTERNAME: clusterName}, client.InNamespace(byoHostPoolScaler.Namespace)); err != nil {
		r.Log.Error(err, "cannot list deployments")
		return err
	}
	for _, md := range mdList.Items {
		lbs := md.GetLabels()
		bmpool, ok := lbs[LABEL_BAREMETALPOOL]
		if !ok {
			err := fmt.Errorf("missing label in kcp %s : %s", md.Name, LABEL_BAREMETALPOOL)
			return err
		}

		initPoolTarget(byoHostPoolTarget, bmpool)

		mdName := md.Name

		err := r.ComputeMachineSetReplicas(ctx, byoHostPoolScaler, mdName, bmpool, byoHostPoolTarget)
		if err != nil {
			r.Log.Error(err, "Cannot compute scale for machine deployment")
			return err
		}
	}
	return nil
}

func (r *ByoHostPoolScalerReconciler) ComputeMachineSetReplicas(
	ctx context.Context,
	byoHostPoolScaler *poolsscalerv1alpha1.ByoHostPoolScaler,
	mdName string,
	bmpool string,
	byoHostPoolTarget *ByoHostPoolTargetType,
) error {
	clusterName := byoHostPoolScaler.Spec.Cluster
	msList := &capi.MachineSetList{}
	if err := r.List(ctx, msList,
		client.MatchingLabels{LABEL_CAPI_CLUSTERNAME: clusterName, LABEL_CAPI_MACHINE_DEPLOYMENT_NAME: mdName},
		client.InNamespace(byoHostPoolScaler.Namespace)); err != nil {
		r.Log.Error(err, "cannot list deployments")
		return err
	}

	for _, ms := range msList.Items {
		(*byoHostPoolTarget)[bmpool][*ms.Spec.Template.Spec.Version] += int32(*ms.Spec.Replicas)
	}

	return nil
}

func (r *ByoHostPoolScalerReconciler) getSha1(inputStr string) (string, error) {
	h := sha1.New()
	_, err := h.Write([]byte(inputStr))
	if err != nil {
		return "", err
	}

	result := hex.EncodeToString(h.Sum(nil))
	return result, err
}

func ownedBy(byoHostPoolScaler *poolsscalerv1alpha1.ByoHostPoolScaler) metav1.OwnerReference {
	return metav1.OwnerReference{
		APIVersion: byoHostPoolScaler.APIVersion,
		Kind:       byoHostPoolScaler.Kind,
		Name:       byoHostPoolScaler.Name,
		UID:        byoHostPoolScaler.UID,
	}
}

func (r *ByoHostPoolScalerReconciler) createByoHostPoolResource(
	ctx context.Context,
	byoHostPoolScaler *poolsscalerv1alpha1.ByoHostPoolScaler,
	byoHostPoolTarget *ByoHostPoolTargetType,
) error {
	var apiServer string
	annotations := byoHostPoolScaler.GetAnnotations()
	value, ok := annotations[apiServerByoHostPoolScalerAnnotation]
	if ok {
		apiServer = value
	} else {
		apiServer = byoHostPoolScaler.Spec.ApiServer
	}

	for bmpool, versionCount := range *byoHostPoolTarget {
		for k8sversion, replicas := range versionCount {

			r.Log.Info(fmt.Sprintf("Generating ByoHostPool resource for baremetalpool %s and k8s version %s with apiServer address %s", bmpool, k8sversion, apiServer))

			originalName := fmt.Sprintf("%s-%s", bmpool, k8sversion)
			sha1Name, err := r.getSha1(originalName)
			if err != nil {
				r.Log.Error(err, "Cannot generate sha1")
				return err
			}

			byoHostPoolName := fmt.Sprintf("byohostpoolscaler-%s-%s", byoHostPoolScaler.Spec.Cluster, sha1Name[0:8])

			byoHostPool := &poolsv1alpha1.ByoHostPool{}
			byoHostPool.ObjectMeta = metav1.ObjectMeta{
				Name:      byoHostPoolName,
				Namespace: byoHostPoolScaler.Namespace,
			}

			_, err = controllerutil.CreateOrUpdate(
				ctx, r.Client, byoHostPool,
				func() error {
					if byoHostPool.ObjectMeta.CreationTimestamp.IsZero() {
						byoHostPool.ObjectMeta.OwnerReferences =
							[]metav1.OwnerReference{ownedBy(byoHostPoolScaler)}
					}
					byoHostPool.Spec.ByohImageFormat = byoHostPoolScaler.Spec.ByohImageFormat
					byoHostPool.Spec.ByohOs = byoHostPoolScaler.Spec.ByohOs
					byoHostPool.Spec.ByohOsVersion = byoHostPoolScaler.Spec.ByohOsVersion
					byoHostPool.Spec.ByohRepository = byoHostPoolScaler.Spec.ByohRepository
					byoHostPool.Spec.K8sVersion = k8sversion
					byoHostPool.Spec.BareMetalPool = bmpool
					byoHostPool.Spec.Replicas = &replicas
					byoHostPool.Spec.ApiServer = apiServer
					byoHostPool.Spec.InsecureSkipTLSVerify = byoHostPoolScaler.Spec.InsecureSkipTLSVerify

					return nil
				})
			if err != nil {
				r.Log.Error(err, "Cannot create ByoHostPool resource")
				return err
			}
		}
	}
	return nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *ByoHostPoolScalerReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&poolsscalerv1alpha1.ByoHostPoolScaler{}).
		Complete(r)
}
